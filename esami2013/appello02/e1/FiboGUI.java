import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.*;

public class FiboGUI extends JFrame {
	
	private static final long serialVersionUID = 1744261895501099952L;
	private final JButton bStart = new JButton("Start");
	private final JButton bNext = new JButton("Next");
	private final JTextField tf = new JTextField(12);
	private DataOutputStream data;
	private DataInputStream input;
	
	public FiboGUI(String path) throws FileNotFoundException {
		this.setSize(400, 100);
		this.setLayout(new FlowLayout());
		this.getContentPane().add(tf);
		this.getContentPane().add(bStart);
		this.getContentPane().add(bNext);
		data = new DataOutputStream(new FileOutputStream(path));
		input = new DataInputStream(new FileInputStream(path));

		this.bNext.setEnabled(false);
		this.bStart.setEnabled(true);
		
		bNext.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e){
				long n = 0;

				if(bNext.isEnabled()) {
					try {
						n = input.readLong();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					if(n != -1) {
						bNext.setText("Next  " + n);
					} else {
						try {
							input.close();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						bNext.setEnabled(false);
					}
				}
			}
				
		});
		
		bStart.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int n = Integer.parseInt(tf.getText());
				Fibonacci fib = new Fibonacci(n);
				try {
					while(fib.hasNext()) {
						data.writeLong(fib.next());
					}
				
					data.writeLong(-1);				
					data.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				
				tf.setEditable(false);
				bNext.setEnabled(true);
				bStart.setEnabled(false);
			}
			
		});
		
		
		
		this.setVisible(true);
	}
	
}

package esami2013.appello05.sol2;

public interface IBinGUIModel {
	
	void one();
	
	void zero();
	
	void reset();
	
	int compute();
	
	String getString();
}
